## Phonegap Mobile App Template using Framework7 and Vuejs 2.0
### 이 Template은 Framework7과 Vuejs 2.0를 기반으로 Phonegap Mobile App를 개발할 수 있습니다.

### Project : Phonegap Mobile App Dev.

#### Installation

1. Install this code on your local system
    
    1. Fork this repository (click 'Fork' button in top right corner)
    2. Clone the forked repository on your local file system
    
        ```
        cd /path/to/install/
        
        git@gitlab.com:xmlyoon/myF7droid.git
        ```  
   
2. Change into directory

    ```
    cd myF7droid
    ```
    
3. Install dependencies

    ```
    npm install
    ```

4. Start project

    ```
    npm run dev
    ```

### Images of Design

<img src="https://gitlab.com/xmlyoon/myF7droid/raw/master/src/assets/f7droid1.png" width="200" height=""></img>
<img src="https://gitlab.com/xmlyoon/myF7droid/raw/master/src/assets/f7droid2.png" width="200" height=""></img>
<img src="https://gitlab.com/xmlyoon/myF7droid/raw/master/src/assets/f7droid3.png" width="200" height=""></img>

Your site will be available at *localhost:[8080]* 


